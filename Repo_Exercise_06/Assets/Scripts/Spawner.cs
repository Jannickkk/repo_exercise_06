﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Spawner : MonoBehaviour
    {
        public int spawnCounter;

        private void Start()
        {
            firstMethod();
        }

        private void firstMethod()
        {
            for (int i = 0; i < spawnCounter; i++)
            {
                GameObject newGo = new GameObject();
                Debug.Log("Something spawned!");
            }
        }

        private void secondMethod(int maxSpawnCounter)
        {
            int i = 0;
            while (i < maxSpawnCounter)
            {
                GameObject newGo = new GameObject();
                Debug.Log("Something spawned!");
                i++;
            }
        }

        private void thirdMethod(int maxSpawnCounter)
        {
            int i = 0;
            do
            {
                GameObject newGo = new GameObject();
                Debug.Log("Something spawned!");
                i++;
            }
            while (i < maxSpawnCounter);
        }
    }
}